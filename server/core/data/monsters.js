module.exports = [{
  // Lich
  id: 1,
  name: 'Lich',
  examine: 'A legendary creature that guards the treature in its lair.',
  graphic: {
    // see /graphics/actors/monsters.png
    row: 0,
    column: 8,
  },
  // TODO: need add max hp and current inorder to be able to fight against monsters

  actions: [
    'examine',
    'atack',
  ],
  // TO DO: This section needs to be addaed to the "spawn"-File
  spawn: {
    x: 10,
    y: 116,
    range: 5,
  },
}];
