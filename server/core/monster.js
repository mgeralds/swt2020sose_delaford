import Socket from '@server/socket';
import UI from 'shared/ui';
import emoji from 'node-emoji';
import monsters from './data/monsters';
import world from './world';

class MONSTER {
    constructor(data) {
            this.id = data.id;
            this.name = data.name;
            // Examine text
            this.examine = data.examine;

            // What actions can be performed on them?
            this.actions = data.actions;

            // Where they will spawn on world restarts
            this.spawn = {
                x: data.spawn.x,
                y: data.spawn.y,
            };
            // Where are they right now?
            this.x = data.spawn.x;
            this.y = data.spawn.y;

            // How far they can walk from spawn location
            this.range = data.spawn.range;

            // When they last performed an action
            this.lastAction = 0;

            // What column they are on in tileset
            this.column = data.graphic.column;
        }
        /**
         * Load the monsters into the game world
         *
         * @param {this} context The server context
         */
    static load(context) {
        monsters.forEach((monster) => {
            world.monsters.push(new MONSTER(monster));
        }, context);

        console.log(`${emoji.get(':space_invader:')} 'Loading MONSTERs...`);
    }

    /**
     * TODO Simulate Monster movement every cycle
     */
    static movement() {
        world.monsters.map((monster) => {
            return monster;
        });

        // Tell the clients of the new monster
        Socket.broadcast('monster:movement', world.monsters);
    }
}

module.exports = MONSTER;
